package concat

import (
	"encoding/json"
	"os"
	"os/exec"
	"strings"

	"github.com/kpango/glg"
)

type InputData struct {
	FileName string `json:"filename"`
	Output   string `json:"output"`
}

func Concat(respInput interface{}, srcPath, dstPath string) error {

	srcPath = strings.TrimSuffix(srcPath, "/")
	dstPath = strings.TrimSuffix(dstPath, "/")

	var inputData InputData
	infoByte, err := json.Marshal(respInput)
	if err != nil {
		glg.Fatalln(err)
	}

	err = json.Unmarshal(infoByte, &inputData)
	if err != nil {
		glg.Fatalln(err)
	}

	command := []string{"-y", "-f", "concat", "-safe", "0", "-i", srcPath + "/" + inputData.FileName, "-c", "copy", dstPath + "/" + inputData.Output}

	glg.Debug(strings.Join(command[:], " "))

	cmd := exec.Command("ffmpeg", command...)
	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	glg.Info("concat content success and output to", inputData.Output)

	return nil
}
